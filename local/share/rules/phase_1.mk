# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

.ONESHELL:

SAMPLES ?=
# min overlap to merge a pair of reads
MIN_OVERLAP ?= 

log:
	mkdir -p $@


1_FASTQ_GZ = $(addsuffix .1.link.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.link.fastq.gz,$(SAMPLES))
# this function install all the links at once
%.link.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(foreach KEY,$(call keys,$(SAMPLE)), \
			$(shell ln -sf $(call get,$(SAMPLE),$(KEY)) $(SAMPLE).$(KEY).link.fastq.gz) \
		) \
	) \
	&& sleep 3


ASSEMBLED = $(addsuffix .assembled.fastq,$(SAMPLES))
%.assembled.fastq: log %.1.link.fastq.gz %.2.link.fastq.gz
	!threads
	$(call load_modules); \
	pear-0.9.6-bin-64 -f $^2 -r $^3 -v $(MIN_OVERLAP) -j $$THREADNUM -o $* \
	2>&1 \
	| tee $</pear.$@.log

# get reads lengths
%.lengths: %.assembled.fastq.gz
	zcat $< \
	| fastq2tab \
	| cut -f1,2 \
	| tab2fasta -s 2 \
	| fasta_length >$@

# read length distribution
DISTRIB_PDF = $(addsuffix .length.distribution.pdf,$(SAMPLES))
%.length.distribution.pdf: %.lengths
	module purge; \
	module load lang/r/3.3; \
	cat <<'EOF' | bRscript -

	library(bitutils)

	options(width=10000)
	setwd("$(PWD)")
	
	lengths = bread(file="$<", header=F)
	colnames(lengths) = c("seq", "len")

	pdf("$@")

	distrib = hist(lengths$$len, plot=F)
	
	barplot(distrib$$counts/sum(distrib$$counts)*100,
	names.arg=distrib$$breaks[-1],
	las=1,
	main="Pear joined reads $* length distribution",
	xlab="Length intervals",
	ylab="% reads in that length interval")
	
	dev.off()
	
	EOF


UNASSEMBLED_FOR = $(addsuffix .unassembled.forward.fastq,$(SAMPLES))
%.unassembled.forward.fastq: %.assembled.fastq
	touch $@

UNASSEMBLED_REW = $(addsuffix .unassembled.reverse.fastq,$(SAMPLES))
%.unassembled.reverse.fastq: %.assembled.fastq
	touch $@

DISCARDED = $(addsuffix .discarded.fastq,$(SAMPLES))
%.discarded.fastq: %.assembled.fastq
	touch $@

# convert to gz ######################

ASSEMBLED_GZ = $(addsuffix .assembled.fastq.gz,$(SAMPLES))
%.assembled.fastq.gz: %.assembled.fastq
	gzip -c <$< >$@

UNASSEMBLED_FOR_GZ = $(addsuffix .unassembled.forward.fastq.gz,$(SAMPLES))
%.unassembled.forward.fastq.gz: %.unassembled.forward.fastq
	gzip -c <$< >$@

UNASSEMBLED_REW_GZ = $(addsuffix .unassembled.reverse.fastq.gz,$(SAMPLES))
%.unassembled.reverse.fastq.gz: %.unassembled.reverse.fastq
	gzip -c <$< >$@

DISCARDED_GZ = $(addsuffix .discarded.fastq.gz,$(SAMPLES))
%.discarded.fastq.gz: %.discarded.fastq
	gzip -c <$< >$@

# analysis assembled reads ###########

ASSEMBLED_STAT = $(addsuffix .assembled.stat,$(SAMPLES))
%.assembled.stat: %.assembled.fastq.gz
	$(call load_modules); \
	nt-fastq-stats --input $< >$@

ASSEMBLED_COUNT = $(addsuffix .assembled.count,$(SAMPLES))
%.assembled.count: %.assembled.stat
	paste \
	<(echo "$*") \
	<(grep "Number_of_reads" $< | cut -f2) \
	<(grep "Min/Avg/Max_length" $< | cut -f 2-) \
	<(bawk 'BEGIN { A=C=G=T=N=TOT=0; } /^1/,/^\s*$$/ { \
	if ( NF == 13) { \
		A+=$$2; C+=$$3; G+=$$4; T+=$$5; N+=$$6; \
		#print $$2,$$3,$$4,$$5,$$6; \
		} \
	} \
	END { TOT=A+C+G+T+N; print TOT,A,C,G,T,N; }' <$<) \
	>$@

# log ##############################

PEAR_LOG_TMP = $(addprefix log/pear.,$(addsuffix .assembled.fastq.log,$(SAMPLES)))
log/pear.%.assembled.fastq.log: %.assembled.fastq
	touch $@


.META: pear.%.log
	1	sample
	2	minimum overlap
	3	total pairs
	4	assembled pairs
	5	not assembled pairs
	6	discarded pairs

PEAR_LOG = $(addprefix pear.,$(addsuffix .log,$(SAMPLES)))
pear.%.log: log/pear.%.assembled.fastq.log
	bawk 'BEGIN { MIN_OVERLAP=""; } !/^[\#+,$$]/ { \
	if ( $$0 ~ /^Minimum overlap/ ) { split($$0,a,": "); MIN_OVERLAP=a[2]; } \
	if ( $$0 ~ /^Assembled reads \./ ) { split($$0,b," "); ASSEMBLED_READS=b[4]; TOT_READS=b[6]; } \
	if ( $$0 ~ /^Discarded reads \./ ) { split($$0,c," "); DISCARDED_READS=c[4]; } \
	if ( $$0 ~ /^Not assembled reads/ ) { split($$0,d," "); NOT_ASSEMBLED_READS=d[5]; } \
	if ( $$0 ~ /^Assembled reads file/ ) { split($$0,e," "); split(e[4],f,"."); FILENAME=f[1]; } \
	} \
	END { print FILENAME, MIN_OVERLAP, TOT_READS, ASSEMBLED_READS, NOT_ASSEMBLED_READS, DISCARDED_READS; }' $< >$@

.META: all.pear.log
	1	sample
	2	minimum overlap
	3	total pairs
	4	assembled pairs
	5	not assembled pairs
	6	discarded pairs
	7	num_reads
	8	min_reads_len
	9	mean_reads_len
	10	max_read_len
	11	num_bases
	12	A
	13	C
	14	G
	15	T
	16	N

all.pear.log: $(PEAR_LOG) $(ASSEMBLED_COUNT)
	cat $(ASSEMBLED_COUNT) \
	| translate -a <(cat $(PEAR_LOG)) 1 >$@

# test flash ##############################

EXTENDED = $(addsuffix .extendedFrags.fastq.gz,$(SAMPLES))
%.extendedFrags.fastq.gz: log %.1.link.fastq.gz %.2.link.fastq.gz
	!threads
	$(call load_modules); \
	flash \
	--threads $$THREADNUM \
	--compress \
	--min-overlap=$(MIN_OVERLAP) \
	--read-len=150 \
	--output-prefix=$* \
	$^2 $^3 \
	2>&1 \
	| tee $</flash.$@.log

NOT_COMBINED1 = $(addsuffix .notCombined_1.fastq.gz,$(SAMPLES))
NOT_COMBINED2 = $(addsuffix .notCombined_2.fastq.gz,$(SAMPLES))
%.notCombined_1.fastq.gz %.notCombined_2.fastq.gz: %.extendedFrags.fastq.gz
	touch $@

FLASH_LOG_TMP = $(addprefix log/flash.,$(addsuffix .extendedFrags.fastq.gz.log,$(SAMPLES)))
log/flash.%.extendedFrags.fastq.gz.log: %.extendedFrags.fastq.gz
	touch $@

.META: flash.%.log
	1	Sample
	2	Min overlap:
	3	Max overlap:
	4	Max mismatch density:
	5	Allow "outie" pairs:
	6	Cap mismatch quals:
	7	Combiner threads:
	8	Input format:
	9	Output format:
	10	Total pairs:
	11	Combined pairs:
	12	Uncombined pairs
	13	Percent combined


FLASH_LOG = $(addprefix flash.,$(addsuffix .log,$(SAMPLES)))
flash.%.log: log/flash.%.extendedFrags.fastq.gz.log
	sed -n '/\[FLASH\]     Min overlap/,/\[FLASH\]     Output format:/p;/\[FLASH]     Total pairs:/,/\[FLASH]     Percent combined:/p' <$< \
	| sed -E 's/\s{2,}/\t/g' \
	| sed -e 's/: /\t/g' \
	| cut -f 3 \
	| transpose \
	| sed -e 's/^/$*\t/' >$@

all.flash.log: $(FLASH_LOG)
	cat $^ >$@

.PHONY: test
test:
	@echo $(FLASH_LOG_TMP)

ALL += log \
	$(EXTENDED) \
	$(FLASH_LOG) \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(ASSEMBLED_GZ) \
	$(UNASSEMBLED_FOR_GZ) \
	$(UNASSEMBLED_REW_GZ) \
	$(DISCARDED_GZ) \
	$(PEAR_LOG) \
	$(ASSEMBLED_COUNT) \
	$(DISTRIB_PDF) \
	all.pear.log

INTERMEDIATE += $(UNASSEMBLED_FOR) \
	$(UNASSEMBLED_REW) \
	$(DISCARDED) \
	$(PEAR_LOG_TMP) \
	$(ASSEMBLED_STAT)

CLEAN += log \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(DISCARDED_GZ)
